import Vue from 'vue'
import Router from 'vue-router'

import IzbiraSobe from '../components/izbiraSobe'
import Risanje from '../components/Risanje'
import GlavnaStran from '../components/GlavnaStran'
import Domov from '../components/Domov'

/* eslint-disable */
Vue.use(Router)
export default new Router({
routes: [
  {
    path: '/',
    name: 'domov',
    component: Domov
  },{
    path: '/izbiraSobe',
    name: 'izbira-sobe',
    component: IzbiraSobe
  },{
    path: '/ustvarjanjeSobe',
    name: 'ustvarjanje-sobe',
    component: IzbiraSobe
  },{
    path: '/:idIgre',
    name: 'izbira-sobe',
    component: GlavnaStran,
    props: true
  }
],
mode: 'history',
scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
        return savedPosition
    } else {
        return { x: 0, y: 0 }
    }
    }
});