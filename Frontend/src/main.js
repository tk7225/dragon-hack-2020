import Vue from 'vue'
import App from './App.vue'
import router from './router/router.js'

require("./../bower_components/jquery/dist/jquery.min.js")
require("./../bower_components/bootstrap/dist/js/bootstrap.min.js")
require("./../bower_components/jquery-slimscroll/jquery.slimscroll.min.js")
require("./../bower_components/fastclick/lib/fastclick.js")
require("./../bower_components/pace/pace.min.js")
require("./../staticno/js/adminlte.min.js")
require("./../staticno/js/demo.js")

Vue.config.productionTip = false

const vm = new Vue({
  render: h => h(App),
  router: router
})
vm.$mount('#app')

Vue.vm = vm
// new Vue({
//   render: h => h(App)
// }).$mount('#app')
