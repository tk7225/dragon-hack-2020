function ikonaTipaIgre (tipIgre) {
  switch (tipIgre) {
    case 'Pustolovščina':
      return 'fa-map-o'
    case 'Humor':
      return 'fa-smile-o'
    case 'Uganke':
      return 'fa-question-circle'
      // return 'fa-user-secret'
    case 'Drugo':
      return 'fa-gamepad'
    default:
      return 'fa-gamepad'
  }
}

function barvaTipaIgre(tipIgre) {
  switch (tipIgre) {
    case "Pustolovščina":
      return "bg-green"
    case "Humor":
      return "bg-yellow"
    case "Uganke":
      return "bg-orange"
    case "Drugo":
      return "bg-teal"
    default:
      return "bg-aqua"
  }
}

function vseVrsteIger() {
  return [
    "Pustolovščina",
    "Humor",
    "Uganke",
    "Drugo"
  ]
}
export default {
  ikonaTipaIgre,
  barvaTipaIgre,
  vseVrsteIger
}