package com.dreamTeam.api.exceptionMappers;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.logging.Level;
import java.util.logging.Logger;

@Provider
public class ForbiddenExceptionMapper implements ExceptionMapper<ForbiddenException> {
    private static final Logger LOG = Logger.getLogger(ForbiddenExceptionMapper.class.getName());

    @Override
    public Response toResponse(ForbiddenException e) {
        LOG.log(Level.FINE, e.getMessage(), e);
        return Response.status(Response.Status.FORBIDDEN)
                .entity(new ExceptionWrapper(e)).build();
    }
}
