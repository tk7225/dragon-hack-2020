package com.dreamTeam.api.resources;

import com.dreamTeam.persistence.datatypes.Story;
import com.dreamTeam.persistence.datatypes.User;
import com.dreamTeam.services.beans.RoomBean;
import com.dreamTeam.services.dto.UserDto;
import com.dreamTeam.services.dto.requests.AddImageRequest;
import com.dreamTeam.services.dto.requests.RoomStateRequest;
import com.dreamTeam.services.dto.requests.StartRoomRequest;
import com.dreamTeam.services.dto.requests.VoteRequest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("rooms")
@RequestScoped
public class RoomResource {

    @Inject
    private RoomBean roomBean;

    @POST
    @Path("create_room")
    public Response createRoom(UserDto adminUser) {
        return Response.ok(roomBean.createRoom(adminUser)).build();
    }

    @POST
    @Path("{roomId}/join_room")
    public Response joinRoom(UserDto user, @PathParam("roomId") String roomLink) {
        return Response.ok(roomBean.joinRoom(user, roomLink)).build();
    }

    @POST
    @Path("{roomId}/add_image")
    public Response addImageToRoom(AddImageRequest addImageRequest, @PathParam("roomId") String roomLink) {
        roomBean.addImageToRoom(roomLink, addImageRequest);
        return Response.ok().build();
    }

    @POST
    @Path("{roomId}/add_story")
    public Response addStory(@PathParam("roomId") String roomLink, Story story) {
        roomBean.addStory(roomLink, story);
        return Response.ok().build();
    }

    @POST
    @Path("{roomId}/vote")
    public Response vote(@PathParam("roomId") String roomLink, VoteRequest voteRequest) {
        roomBean.voteForStory(roomLink, voteRequest);
        return Response.ok().build();
    }

    @POST
    @Path("{roomId}/start")
    public Response startRoom(@PathParam("roomId") String roomLink, StartRoomRequest startRoomRequest) {
        roomBean.startRoom(roomLink, startRoomRequest);
        return Response.ok().build();
    }

    @POST
    @Path("{roomId}/game_state")
    public Response getRoomState(@PathParam("roomId") String roomLink, RoomStateRequest roomStateRequest) {
        return Response.ok(roomBean.getRoomState(roomLink, roomStateRequest)).build();
    }
}
