package com.dreamTeam.api;

import com.kumuluz.ee.cors.annotations.CrossOrigin;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("v1")
@CrossOrigin(supportedMethods = "GET,POST,PUT,DELETE,HEAD,OPTIONS")
public class APIv1 extends Application {
}
