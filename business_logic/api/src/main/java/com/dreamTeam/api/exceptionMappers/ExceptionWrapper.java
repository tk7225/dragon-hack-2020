package com.dreamTeam.api.exceptionMappers;

public class ExceptionWrapper {

    private final String message;

    public ExceptionWrapper(Exception exception) {
        this.message = exception.toString();
    }

    public String getMessage() {
        return message;
    }
}
