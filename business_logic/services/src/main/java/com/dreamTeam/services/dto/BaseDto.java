package com.dreamTeam.services.dto;

import javax.json.bind.annotation.JsonbProperty;

public abstract class BaseDto {
    @JsonbProperty
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
