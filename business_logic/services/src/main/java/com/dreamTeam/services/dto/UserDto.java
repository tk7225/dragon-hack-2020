package com.dreamTeam.services.dto;

import com.dreamTeam.persistence.datatypes.User;

import javax.json.bind.annotation.JsonbProperty;

public class UserDto extends BaseDto {
    @JsonbProperty("ime")
    private String name;
    @JsonbProperty
    private Boolean isAdmin;

    public UserDto(){
        super();
    }

    public UserDto(User user) {
        this();
        this.setName(user.getName());
        this.setId(user.getId());
    }

    public User toUser(){
        User user = new User();
        user.setId(getId());
        user.setName(getName());
        return user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }
}
