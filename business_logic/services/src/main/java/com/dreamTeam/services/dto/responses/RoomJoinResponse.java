package com.dreamTeam.services.dto.responses;

public class RoomJoinResponse {

    private final String userId;

    public RoomJoinResponse(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }
}
