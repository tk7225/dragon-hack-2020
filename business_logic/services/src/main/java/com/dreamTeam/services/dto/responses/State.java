package com.dreamTeam.services.dto.responses;

import com.dreamTeam.persistence.datatypes.Image;
import com.dreamTeam.persistence.datatypes.Story;

import javax.json.bind.annotation.JsonbProperty;
import java.util.Collection;
import java.util.HashSet;

public class State {
    // common
    @JsonbProperty("preostaliCas")
    private Long remainingTimeInSeconds;

    // phase 1
    @JsonbProperty("trenutnaSlika")
    private Integer currentImage;
    @JsonbProperty("navdihZaSliko")
    private String imageInspiration;
    @JsonbProperty("steviloSlik")
    private Integer minImagesToDraw;

    // phase 2
    @JsonbProperty("steviloUporabljenihSlik")
    private Integer minImagesToUse;
    @JsonbProperty("slike")
    private Collection<Image> images;

    // phase 3
    @JsonbProperty("zgodbe")
    private Collection<Story> stories;

    public State() {
        images = new HashSet<>();
        stories = new HashSet<>();
    }


    public Long getRemainingTimeInSeconds() {
        return remainingTimeInSeconds;
    }

    public void setRemainingTimeInSeconds(Long remainingTimeInSeconds) {
        this.remainingTimeInSeconds = remainingTimeInSeconds;
    }

    public Integer getCurrentImage() {
        return currentImage;
    }

    public void setCurrentImage(Integer currentImage) {
        this.currentImage = currentImage;
    }

    public String getImageInspiration() {
        return imageInspiration;
    }

    public void setImageInspiration(String imageInspiration) {
        this.imageInspiration = imageInspiration;
    }

    public Integer getMinImagesToDraw() {
        return minImagesToDraw;
    }

    public void setMinImagesToDraw(Integer minImagesToDraw) {
        this.minImagesToDraw = minImagesToDraw;
    }

    public Integer getMinImagesToUse() {
        return minImagesToUse;
    }

    public void setMinImagesToUse(Integer minImagesToUse) {
        this.minImagesToUse = minImagesToUse;
    }

    public Collection<Image> getImages() {
        return images;
    }

    public void setImages(Collection<Image> images) {
        this.images = images;
    }

    public Collection<Story> getStories() {
        return stories;
    }

    public void setStories(Collection<Story> stories) {
        this.stories = stories;
    }
}
