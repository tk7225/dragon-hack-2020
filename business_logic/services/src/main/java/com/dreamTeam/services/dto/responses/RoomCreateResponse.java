package com.dreamTeam.services.dto.responses;

public class RoomCreateResponse {
    private final String userId;
    private final String roomLinkShare;

    public RoomCreateResponse(String roomLinkShare, String userId) {
        this.roomLinkShare = roomLinkShare;
        this.userId = userId;
    }

    public String getRoomLinkShare() {
        return roomLinkShare;
    }

    public String getUserId() {
        return userId;
    }
}
