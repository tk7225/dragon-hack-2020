package com.dreamTeam.services.dto.requests;

public class StartRoomRequest {
    private String adminUserId;

    public String getAdminUserId() {
        return adminUserId;
    }

    public void setAdminUserId(String adminUserId) {
        this.adminUserId = adminUserId;
    }
}
