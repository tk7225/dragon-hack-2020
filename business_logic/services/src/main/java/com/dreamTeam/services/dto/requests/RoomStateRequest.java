package com.dreamTeam.services.dto.requests;

import javax.json.bind.annotation.JsonbProperty;

public class RoomStateRequest {
    @JsonbProperty("idUporabnika")
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
