package com.dreamTeam.services.dto.requests;

import javax.json.bind.annotation.JsonbProperty;

public class VoteRequest {
    @JsonbProperty("idUporabnika")
    private String voterId;
    @JsonbProperty("idZgodbe")
    private String storyId;

    public String getVoterId() {
        return voterId;
    }

    public void setVoterId(String voterId) {
        this.voterId = voterId;
    }

    public String getStoryId() {
        return storyId;
    }

    public void setStoryId(String storyId) {
        this.storyId = storyId;
    }
}
