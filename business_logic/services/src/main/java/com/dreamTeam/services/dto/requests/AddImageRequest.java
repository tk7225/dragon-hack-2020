package com.dreamTeam.services.dto.requests;

import javax.json.bind.annotation.JsonbProperty;

public class AddImageRequest {
    @JsonbProperty("slikaBase64")
    private String imageBase64;
    @JsonbProperty("slikaPoteze")
    private String imageStrokes;
    @JsonbProperty("idUporabnika")
    private String userId;

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    public String getImageStrokes() {
        return imageStrokes;
    }

    public void setImageStrokes(String imageStrokes) {
        this.imageStrokes = imageStrokes;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
