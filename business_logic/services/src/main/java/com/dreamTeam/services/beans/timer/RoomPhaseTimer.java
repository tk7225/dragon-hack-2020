package com.dreamTeam.services.beans.timer;

import com.dreamTeam.persistence.datatypes.Room;
import com.dreamTeam.services.beans.RoomBean;
import com.dreamTeam.services.database.Database;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.xml.crypto.Data;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

@ApplicationScoped
public class RoomPhaseTimer {

    private static final Logger LOG = Logger.getLogger(RoomPhaseTimer.class.getName());
    private Map<Integer, Duration> phaseDurations;

    @Inject
    private Database database;

    private ScheduledExecutorService roomPhaseExecutor;

    private void init(@Observes @Initialized(ApplicationScoped.class) Object event) {
        LOG.info("Initialising logging service");
        phaseDurations = new HashMap<>();
        phaseDurations.put(1, Duration.ofSeconds(120));
        phaseDurations.put(2, Duration.ofSeconds(180));
        phaseDurations.put(3, Duration.ofSeconds(120));

        roomPhaseExecutor = Executors.newSingleThreadScheduledExecutor();
        roomPhaseExecutor.scheduleAtFixedRate(this::updateRoomPhases, 0, 1, TimeUnit.SECONDS);
    }

    @PreDestroy
    private void destroy() {
        roomPhaseExecutor.shutdown();
    }

    private void updateRoomPhases() {
        try {
            Collection<Room> allRooms = database.getAllRooms();

            for (Room room : allRooms) {
                if (room.getGamePhase() == 0 || room.getGamePhase() == 4) {
                    continue;
                }

                Instant roomPhaseStart = room.getLastPhaseStart();
                Instant phaseEnd = roomPhaseStart.plus(phaseDurations.get(room.getGamePhase()));

                if (phaseEnd.isBefore(Instant.now())) {
                    room.nextGamePhase();
                }
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Unknown exception", e);
        }
    }

    public Duration getPhaseDuration(int phaseIndex) {
        return phaseDurations.get(phaseIndex);
    }
}
