package com.dreamTeam.services.beans;

import com.dreamTeam.persistence.datatypes.*;
import com.dreamTeam.services.beans.timer.RoomPhaseTimer;
import com.dreamTeam.services.database.Database;
import com.dreamTeam.services.dto.UserDto;
import com.dreamTeam.services.dto.requests.AddImageRequest;
import com.dreamTeam.services.dto.requests.RoomStateRequest;
import com.dreamTeam.services.dto.requests.StartRoomRequest;
import com.dreamTeam.services.dto.requests.VoteRequest;
import com.dreamTeam.services.dto.responses.GameStateResponse;
import com.dreamTeam.services.dto.responses.RoomCreateResponse;
import com.dreamTeam.services.dto.responses.RoomJoinResponse;
import com.dreamTeam.services.dto.responses.State;
import com.dreamTeam.services.topicGenerator.TopicGenerator;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.ForbiddenException;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@RequestScoped
public class RoomBean {

    @Inject
    private Database database;

    @Inject
    private TopicGenerator topicGenerator;

    @Inject
    private RoomPhaseTimer roomPhaseTimer;

    @Inject
    private ClassifierBean classifierBean;

    public RoomCreateResponse createRoom(UserDto adminUser) {
        User savedUser = database.saveUser(adminUser.toUser());

        Room room = new Room();
        room.setAdminUser(savedUser);
        room.getJoinedUsers().add(savedUser);
        room.setRoomTopic(topicGenerator.getRandomTopic());

        Room savedRoom = database.saveRoom(room);
        return new RoomCreateResponse(savedRoom.getRoomShareLink(), savedUser.getId());
    }

    public RoomJoinResponse joinRoom(UserDto user, String shareLink) {
        Room savedRoom = database.getRoomByShareLink(shareLink).orElseThrow();
        User savedUser = database.saveUser(user.toUser());
        savedRoom.getJoinedUsers().add(savedUser);

        return new RoomJoinResponse(savedUser.getId());
    }

    public void addImageToRoom(String roomLink, AddImageRequest addImageRequest) {
        Room savedRoom = database.getRoomByShareLink(roomLink).orElseThrow();
        User savedUser = database.getUserById(addImageRequest.getUserId()).orElseThrow();

        if (savedRoom.getUsersImages().get(savedUser).size() > savedRoom.getNumberOfImages()) {
            throw new ForbiddenException("Already drew all images");
        }

        Image addedImage = new Image();
        addedImage.setImageBase64(addImageRequest.getImageBase64());
        addedImage.setImageStrokes(addImageRequest.getImageStrokes());
        addedImage.setDetection(classifierBean.getClassification(addedImage));
        database.addImageToRoom(addedImage, savedRoom, savedUser);
        checkRoomNextStateRequirements(savedRoom);
    }

    public void startRoom(String roomLinkId, StartRoomRequest startRoomRequest) {
        Room savedRoom = database.getRoomByShareLink(roomLinkId).orElseThrow();

        if (!savedRoom.getAdminUser().getId().equals(startRoomRequest.getAdminUserId())) {
            throw new ForbiddenException("Not allowed to start a non owned room");
        }

        savedRoom.nextGamePhase();
    }

    public GameStateResponse getRoomState(String roomLink, RoomStateRequest roomStateRequest) {
        User user = database.getUserById(roomStateRequest.getUserId()).orElseThrow();
        Room savedRoom = database.getRoomByShareLink(roomLink).orElseThrow();

        if (!savedRoom.getJoinedUsers().contains(user)) {
            throw new ForbiddenException("User is not allowed to get states of non joined rooms");
        }

        Collection<UserDto> playerDTOs = savedRoom.getJoinedUsers().stream().map(UserDto::new).collect(Collectors.toSet());
        UserDto admin = playerDTOs.stream()
                .filter(userDto -> userDto.getId().equals(savedRoom.getAdminUser().getId())).findAny().orElseThrow();
        admin.setAdmin(true);
        int gamePhase = savedRoom.getGamePhase();

        State state = new State();
        GameStateResponse response = new GameStateResponse();
        response.setGameStateIndex(gamePhase);
        response.setState(state);
        response.setUsers(playerDTOs);

        Instant phaseEnd = savedRoom.getLastPhaseStart().plus(roomPhaseTimer.getPhaseDuration(1));
        Duration remainingTime = Duration.between(Instant.now(), phaseEnd);

        Map<User, Collection<Image>> allUserImagesMap = savedRoom.getUsersImages();
        Collection<Image> imageCollection = allUserImagesMap.values().stream()
                .flatMap(Collection::stream).collect(Collectors.toList());
        state.setImageInspiration(savedRoom.getRoomTopic());
        state.setRemainingTimeInSeconds(remainingTime.getSeconds());

        if (gamePhase == 0){
            state.setMinImagesToDraw(savedRoom.getNumberOfImages());
        } else if (gamePhase == 1) {
            savedRoom.getUsersImages().putIfAbsent(user, new HashSet<>());
            int numberOfCurrentUserImages = savedRoom.getUsersImages().get(user).size();

            state.setCurrentImage(numberOfCurrentUserImages);
            state.setMinImagesToDraw(savedRoom.getNumberOfImages());
            int numberOfImages = imageCollection.size();

            state.setMinImagesToUse(Math.min(numberOfImages, savedRoom.getMinNumberOfImagesToUse()));
            state.setImageInspiration(savedRoom.getRoomTopic());
        } else if (gamePhase == 2) {
            state.setImages(imageCollection);
            state.setMinImagesToUse(savedRoom.getMinNumberOfImagesToUse());
        } else if (gamePhase == 3) {
            Collection<Story> storyCollection = savedRoom.getUsersStories().values();

            for (Story story : storyCollection) {
                for (StoryWord storyWord : story.getBesedilo()) {
                    if (storyWord.getTip().equals("slika")) {
                        Image image = database.getImageById(storyWord.getId()).orElseThrow();
                        storyWord.setText(image.getDetection());
                    }
                }
            }
            state.setStories(storyCollection);
        } else if (gamePhase == 4) {
            Collection<Story> storyCollection = savedRoom.getUsersStories().values();
            state.setStories(storyCollection);
        } else {
            throw new RuntimeException("Unknown phase");
        }

        return response;
    }

    public void addStory(String roomLink, Story story) {
        Room savedRoom = database.getRoomByShareLink(roomLink).orElseThrow();
        User savedUser = database.getUserById(story.getIdUporabnika()).orElseThrow();

        if (!savedRoom.getJoinedUsers().contains(savedUser)) {
            throw new ForbiddenException("User does not belong to the room");
        }

        if (savedRoom.getUsersStories().containsKey(savedUser)) {
            throw new ForbiddenException("User already posted a story");
        }

        story.setIdUporabnika(savedUser.getName());
        database.addStoryToRoom(savedRoom, savedUser, story);
        checkRoomNextStateRequirements(savedRoom);
    }

    public void voteForStory(String roomLink, VoteRequest voteRequest) {
        Room savedRoom = database.getRoomByShareLink(roomLink).orElseThrow();
        User votingUser = database.getUserById(voteRequest.getVoterId()).orElseThrow();

        if (!savedRoom.getJoinedUsers().contains(votingUser)) {
            throw new ForbiddenException("User does not belong to the room");
        }

        if (savedRoom.getUsersThatAlreadyVoted().contains(votingUser)) {
            throw new ForbiddenException("User already voted");
        }

        savedRoom.getUsersThatAlreadyVoted().add(votingUser);
        Story savedStory = database.getStoryById(voteRequest.getStoryId()).orElseThrow();
        savedStory.voteForStory();
        checkRoomNextStateRequirements(savedRoom);
    }

    private void checkRoomNextStateRequirements(Room room) {
        boolean satisfies = true;

        if (room.getGamePhase() == 1) {
            for (Collection<Image> value : room.getUsersImages().values()) {
                int size = value.size();
                if (size < room.getNumberOfImages()) {
                    satisfies = false;
                    break;
                }
            }
        } else if (room.getGamePhase() == 2) {
            if (room.getUsersStories().size() < room.getJoinedUsers().size()) {
                satisfies = false;
            }
        } else if (room.getGamePhase() == 3) {
            if (room.getUsersThatAlreadyVoted().size() < room.getJoinedUsers().size()) {
                satisfies = false;
            }
        }

        if (satisfies) {
            room.nextGamePhase();
        }
    }
}
