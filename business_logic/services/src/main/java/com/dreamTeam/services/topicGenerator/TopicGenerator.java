package com.dreamTeam.services.topicGenerator;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@ApplicationScoped
public class TopicGenerator {
    private List<String> topicList;
    private Random random;

    @PostConstruct
    private void init() {
        random = new Random();

        topicList = new ArrayList<>();
        topicList.add("fantazija");
        topicList.add("predmeti so oživeli");
        topicList.add("drevesa so prijatelji");
        topicList.add("slovenski pregovori");
    }

    public String getRandomTopic() {
        return topicList.get(random.nextInt(topicList.size()));
    }
}
