package com.dreamTeam.services.dto.responses;

import com.dreamTeam.services.dto.UserDto;

import javax.json.bind.annotation.JsonbProperty;
import java.util.Collection;
import java.util.HashSet;

public class GameStateResponse {

    @JsonbProperty("fazaIgre")
    private Integer gameStateIndex;
    @JsonbProperty("stanje")
    private State state;
    @JsonbProperty("igralci")
    private Collection<UserDto> users;

    public GameStateResponse() {
        users = new HashSet<>();
    }

    public Integer getGameStateIndex() {
        return gameStateIndex;
    }

    public void setGameStateIndex(Integer gameStateIndex) {
        this.gameStateIndex = gameStateIndex;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Collection<UserDto> getUsers() {
        return users;
    }

    public void setUsers(Collection<UserDto> users) {
        this.users = users;
    }
}
