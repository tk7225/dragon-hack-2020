package com.dreamTeam.services.database;

import com.dreamTeam.persistence.datatypes.*;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ForbiddenException;
import java.util.*;

@ApplicationScoped
public class Database {
    private Map<String, Room> activeRooms;
    private Map<String, User> activeUsers;
    private Map<String, Image> activeImages;
    private Map<String, Story> activeStories;

    @PostConstruct
    private void init() {
        activeRooms = new HashMap<>();
        activeUsers = new HashMap<>();
        activeImages = new HashMap<>();
        activeStories = new HashMap<>();
    }

    public Room saveRoom(Room room) {
        room.setId(getUniqueId(activeRooms));

        boolean unique = false;
        String randomRoomLink = UUID.randomUUID().toString().substring(0, 4);

        while (!unique) {
            for (Room value : activeRooms.values()) {
                if (value.getRoomShareLink().equals(randomRoomLink)) {
                    randomRoomLink = UUID.randomUUID().toString();
                    break;
                }
            }
            unique = true;
        }
        room.setRoomShareLink(randomRoomLink);
        activeRooms.put(room.getId(), room);

        return room;
    }

    public Collection<Room> getAllRooms() {
        return activeRooms.values();
    }

    public Optional<Room> getRoomById(String roomId) {
         return Optional.ofNullable(activeRooms.get(roomId));
    }

    public Optional<Room> getRoomByShareLink(String shareLink) {
        return activeRooms.values().stream()
                .filter(room -> room.getRoomShareLink().equals(shareLink))
                .findAny();
    }

    public User saveUser(User user) {
        user.setId(getUniqueId(activeUsers));
        activeUsers.put(user.getId(), user);
        return user;
    }

    public Optional<User> getUserById(String userId) {
        return Optional.ofNullable(activeUsers.get(userId));
    }

    public void addImageToRoom(Image image, Room room, User user) {
        image.setId(getUniqueId(activeImages));
        activeImages.put(image.getId(), image);
        Collection<Image> usersImages = room.getUsersImages().computeIfAbsent(user, k -> new HashSet<>());
        if (usersImages.size() >= room.getNumberOfImages()) {
            throw new IllegalArgumentException("Can not add any more images for this user");
        }
        room.getUsersImages().get(user).add(image);
    }

    public Optional<Image> getImageById(String imageId) {
        return Optional.ofNullable(activeImages.get(imageId));
    }

    public void addStoryToRoom(Room room, User user, Story story) {
        if (room.getUsersStories().containsKey(user)) {
            throw new ForbiddenException("User already created a story");
        }

        story.setId(getUniqueId(activeStories));
        activeStories.put(story.getId(), story);
        room.getUsersStories().put(user,story);
    }

    public Optional<Story> getStoryById(String storyId) {
        return Optional.ofNullable(activeStories.get(storyId));
    }

    private String getUniqueId(Map<String, ? extends BasicEntity> entityMap) {
        boolean unique = false;
        String randomId = UUID.randomUUID().toString();

        while (!unique) {
            for (BasicEntity value : entityMap.values()) {
                if (value.getId().equals(randomId)) {
                    randomId = UUID.randomUUID().toString();
                    break;
                }
            }
            unique = true;
        }

        return randomId;
    }
}
