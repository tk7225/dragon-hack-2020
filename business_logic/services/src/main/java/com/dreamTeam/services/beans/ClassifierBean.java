package com.dreamTeam.services.beans;

import com.dreamTeam.persistence.datatypes.Image;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

@RequestScoped
public class ClassifierBean {
    private static final String CLASSIFICATION_ENDPOINT = "http://localhost:8000";

    private final Client client = ClientBuilder.newClient();

    public String getClassification(Image image) {
        Response response = client.target(CLASSIFICATION_ENDPOINT)
                .request(MediaType.APPLICATION_OCTET_STREAM_TYPE)
                .post(Entity.text(image.getImageStrokes().getBytes()));
        return response.readEntity(String.class);
    }
}
