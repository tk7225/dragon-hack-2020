package com.dreamTeam.persistence.datatypes;

public abstract class BasicEntity {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
