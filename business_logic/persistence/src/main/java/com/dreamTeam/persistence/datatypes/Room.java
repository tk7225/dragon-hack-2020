package com.dreamTeam.persistence.datatypes;

import java.time.Instant;
import java.util.*;

public class Room extends BasicEntity {
    private String roomShareLink;
    private User adminUser;
    private String roomTopic;
    private final List<User> joinedUsers;
    private Integer gamePhase;
    private Instant lastPhaseStart;

    // used for phase 1
    private Integer numberOfImages;

    // used for phase 2
    private final Map<User, Collection<Image>> usersImages;
    private Integer minNumberOfImagesToUse;

    // used for phase 3
    private final Map<User, Story> usersStories;
    private final Set<User> usersThatAlreadyVoted;

    public Room() {
        super();
        joinedUsers = new ArrayList<>();
        usersImages = new HashMap<>();
        usersStories = new HashMap<>();
        usersThatAlreadyVoted = new HashSet<>();

        gamePhase = 0;
        numberOfImages = 1;
        minNumberOfImagesToUse = 3;
        lastPhaseStart = Instant.now();
    }

    public void nextGamePhase() {
        lastPhaseStart = Instant.now();
        gamePhase = gamePhase + 1;
    }

    public List<User> getJoinedUsers() {
        return joinedUsers;
    }

    public String getRoomShareLink() {
        return roomShareLink;
    }

    public void setRoomShareLink(String roomShareLink) {
        this.roomShareLink = roomShareLink;
    }

    public User getAdminUser() {
        return adminUser;
    }

    public void setAdminUser(User adminUser) {
        this.adminUser = adminUser;
    }

    public Map<User, Collection<Image>> getUsersImages() {
        return usersImages;
    }

    public Integer getGamePhase() {
        return gamePhase;
    }

    public void setGamePhase(Integer gamePhase) {
        this.gamePhase = gamePhase;
    }

    public String getRoomTopic() {
        return roomTopic;
    }

    public void setRoomTopic(String roomTopic) {
        this.roomTopic = roomTopic;
    }

    public Instant getLastPhaseStart() {
        return lastPhaseStart;
    }

    public void setLastPhaseStart(Instant lastPhaseStart) {
        this.lastPhaseStart = lastPhaseStart;
    }

    public Integer getNumberOfImages() {
        return numberOfImages;
    }

    public void setNumberOfImages(Integer numberOfImages) {
        this.numberOfImages = numberOfImages;
    }

    public Integer getMinNumberOfImagesToUse() {
        return minNumberOfImagesToUse;
    }

    public void setMinNumberOfImagesToUse(Integer minNumberOfImagesToUse) {
        this.minNumberOfImagesToUse = minNumberOfImagesToUse;
    }

    public Map<User, Story> getUsersStories() {
        return usersStories;
    }

    public Set<User> getUsersThatAlreadyVoted() {
        return usersThatAlreadyVoted;
    }
}
