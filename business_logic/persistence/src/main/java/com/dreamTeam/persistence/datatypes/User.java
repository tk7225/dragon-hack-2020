package com.dreamTeam.persistence.datatypes;

public class User extends BasicEntity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
