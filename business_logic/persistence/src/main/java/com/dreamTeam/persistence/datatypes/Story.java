package com.dreamTeam.persistence.datatypes;

import java.util.LinkedList;
import java.util.List;

public class Story extends BasicEntity {
    private String idUporabnika;
    private String imeUporabnika;
    private List<StoryWord> besedilo;
    private int steviloGlasov;

    public Story() {
        super();
        besedilo = new LinkedList<>();
        steviloGlasov = 0;
    }

    public void voteForStory() {
        steviloGlasov = steviloGlasov + 1;
    }

    public String getIdUporabnika() {
        return idUporabnika;
    }

    public void setIdUporabnika(String idUporabnika) {
        this.idUporabnika = idUporabnika;
    }

    public List<StoryWord> getBesedilo() {
        return besedilo;
    }

    public void setBesedilo(List<StoryWord> besedilo) {
        this.besedilo = besedilo;
    }

    public int getSteviloGlasov() {
        return steviloGlasov;
    }

    public void setSteviloGlasov(int steviloGlasov) {
        this.steviloGlasov = steviloGlasov;
    }

    public String getImeUporabnika() {
        return imeUporabnika;
    }

    public void setImeUporabnika(String imeUporabnika) {
        this.imeUporabnika = imeUporabnika;
    }
}
