package com.dreamTeam.persistence.datatypes;

public class Image extends BasicEntity{
    private String imageBase64;
    private String imageStrokes;
    private String detection;

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    public String getImageStrokes() {
        return imageStrokes;
    }

    public void setImageStrokes(String imageStrokes) {
        this.imageStrokes = imageStrokes;
    }

    public String getDetection() {
        return detection;
    }

    public void setDetection(String detection) {
        this.detection = detection;
    }
}
