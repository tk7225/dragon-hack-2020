import os
import numpy as np
from keras.preprocessing.sequence import pad_sequences
from sklearn.preprocessing import LabelEncoder
from keras.metrics import top_k_categorical_accuracy
from ast import literal_eval
from keras.models import Sequential
from keras.layers import BatchNormalization, Conv1D, LSTM, Dense, Dropout

STROKE_COUNT = 196

def get_available_gpus():
    from tensorflow.python.client import device_lib
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']

def top_3_accuracy(x,y): return top_k_categorical_accuracy(x,y, 3)

def _stack_it(raw_strokes):
    """preprocess the string and make
    a standard Nx3 stroke vector"""
    stroke_vec = literal_eval(raw_strokes) # string->list
    # unwrap the list
    in_strokes = [(xi,yi,i)
     for i,(x,y) in enumerate(stroke_vec)
     for xi,yi in zip(x,y)]
    c_strokes = np.stack(in_strokes)
    # replace stroke id with 1 for continue, 2 for new
    c_strokes[:,2] = [1]+np.diff(c_strokes[:,2]).tolist()
    c_strokes[:,2] += 1 # since 0 is no stroke
    # pad the strokes with zeros
    return pad_sequences(c_strokes.swapaxes(0, 1),
                         maxlen=STROKE_COUNT,
                         padding='post').swapaxes(0, 1)


def get_labels(dir):
    words = []
    with open(os.path.join('.', dir)) as f:
        for line in f:
            # print(line)
            words.append(line.strip())
    # print(words)
    return words

def initialize_model():
    #get labels
    word_encoder = LabelEncoder()
    word_encoder.fit(get_labels("labels.txt"))

    # our model structure
    # if len(get_available_gpus())>0:
    #     # https://twitter.com/fchollet/status/918170264608817152?lang=en
    #     from keras.layers import CuDNNLSTM as LSTM # this one is about 3x faster on GPU instances
    stroke_read_model = Sequential()
    stroke_read_model.add(BatchNormalization(input_shape = (None, ) + (3,)))
    # filter count and length are taken from the script https://github.com/tensorflow/models/blob/master/tutorials/rnn/quickdraw/train_model.py
    stroke_read_model.add(Conv1D(48, (5,)))
    stroke_read_model.add(Dropout(0.3))
    stroke_read_model.add(Conv1D(64, (5,)))
    stroke_read_model.add(Dropout(0.3))
    stroke_read_model.add(Conv1D(96, (3,)))
    stroke_read_model.add(Dropout(0.3))
    stroke_read_model.add(LSTM(128, return_sequences = True))
    stroke_read_model.add(Dropout(0.3))
    stroke_read_model.add(LSTM(128, return_sequences = False))
    stroke_read_model.add(Dropout(0.3))
    stroke_read_model.add(Dense(512))
    stroke_read_model.add(Dropout(0.3))
    stroke_read_model.add(Dense(340, activation = 'softmax'))
    stroke_read_model.compile(optimizer = 'adam',
                              loss = 'categorical_crossentropy',
                              metrics = ['categorical_accuracy', top_3_accuracy])
    #stroke_read_model.summary()

    # read model
    weight_path=os.path.join('.', "{}_weights.best.hdf5".format('stroke_lstm_model'))
    stroke_read_model.load_weights(weight_path)
    return stroke_read_model, word_encoder

def make_prediction(stroke_read_model, word_encoder, drawing):
    sub_vec = np.stack([np.array(_stack_it(drawing))], 0)
    sub_pred = stroke_read_model.predict(sub_vec)
    pred = [word_encoder.classes_[np.argsort(-1 * c_pred)[:1]] for c_pred in sub_pred]
    #print(pred)
    return pred[0][0]


def translate(word):
    translations = dict()
    with open("prevodi.txt") as f:
        for line in f:
            english, slovene = line.strip().split(",")
            translations[english] = slovene
    return translations[word]

if __name__ == "__main__":
    drawing = "[[[17, 18, 20, 25, 137, 174, 242, 249, 251, 255, 251, 229, 193, 166, 104, 58, 25, 13, 3], [117, 176, 184, 185, 185, 190, 191, 187, 179, 122, 114, 103, 103, 109, 109, 100, 98, 103, 112]], [[64, 39, 25, 24, 37, 73, 78, 88, 91, 91, 84], [117, 117, 134, 155, 177, 180, 176, 160, 148, 129, 127]], [[203, 188, 181, 175, 174, 188, 207, 219, 225, 226, 215], [122, 120, 127, 137, 160, 169, 173, 161, 145, 133, 128]], [[110, 111, 151, 154, 154, 143, 108], [133, 150, 151, 150, 130, 127, 128]], [[0, 7, 18, 20, 28], [0, 10, 59, 80, 100]]]"
    stroke_read_model, word_encoder = initialize_model()
    print(make_prediction(stroke_read_model, word_encoder, drawing))