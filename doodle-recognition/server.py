from http.server import HTTPServer, BaseHTTPRequestHandler
import doodle_recognition_lstm as drlstm
from io import BytesIO


stroke_read_model, word_encoder = drlstm.initialize_model()

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        body = str(self.rfile.read(content_length),'utf-8')
        response = BytesIO()
        try:
            prediction = drlstm.make_prediction(stroke_read_model, word_encoder, body)
            prediction = drlstm.translate(prediction)
            self.send_response(200)
            self.end_headers()
            response.write(bytes(prediction, 'utf-8'))

        except:
            self.send_response(400)
            self.end_headers()
            response.write(b"Incorrect input.")

        self.wfile.write(response.getvalue())


httpd = HTTPServer(('localhost', 8000), SimpleHTTPRequestHandler)
httpd.serve_forever()